# @name         Hash table sort filters by keys or content size
# @version      1.0.1
# @author       Darathor < darathor@free.fr >
# @supportURL   https://framagit.org/Darathor/jekyll-hash-table-sort-filters/issues
# @license      WTFPL
#
# Inspired on: https://www.codeofclimber.ru/2015/sorting-site-tags-in-jekyll/

require "i18n"

module Jekyll
  module HashTableSortFilter
    def sort_by_keys(hashTable)
      return hashTable.sort_by { |x| [ I18n.transliterate(x[0]).downcase ] }
    end

    def sort_by_content_size(hashTable)
      return hashTable.sort_by { |x| [ 0 - x[1].size, I18n.transliterate(x[0]).downcase ] }
    end
  end
end

Liquid::Template.register_filter(Jekyll::HashTableSortFilter)