# jekyll-hash-table-sort-filters

## Français

Il s'agit d'un plugin très simple pour [Jekyll](https://jekyllrb.com/) qui ajoute deux nouveau filtres de tri de tableaux associatifs (dans mon cas je l'ai utilisé pour trier ma liste de catégories) utilisables dans les templates Liquid :
 * `sort_by_keys` : trie le tableau selon ses clés, en ignorant la casse et les accents
 * `sort_by_content_size` : trie le tableau selon la taille de son contenu (en via la propriété <code>size</code>) et, en cas d'égalité selon ses clés, en ignorant la casse et les accents 

### Installation

Copier le fichier `_plugins/hash_table_sort_filters.rb` dans le répertoire `_plugins` de votre site Jekyll et relancer le serveur Jekyll.

### Utilisation

Exemple 1 : affichage d'un menu des catégories du site triées par ordre alphabétique.
```
<h2>Catégories</h2>
<ul>
  {% assign categories = site.categories | sort_by_keys %}
  {% for category in categories %}
    <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
  {% endfor %}
</ul>
```

Exemple 2 : le même menu trié par nombre d'articles dans la catégorie. 
```
<h2>Catégories</h2>
<ul>
  {% assign categories = site.categories | sort_by_content_size %}
  {% for category in categories %}
    <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
  {% endfor %}
</ul>
```

## Anglais

This is a very simple plugin for [Jekyll](https://jekyllrb.com/) that adds two new filters to sort hash tables (I use it to sort my list of categories) usable in Liquid templates:
 * `sort_by_keys`: sorts the hash table by keys, ignoring case and accents
 * `sort_by_content_size`: sorts the hash table by the content size (using the property <code>size</code>) and, in case of equality by case keys, ignoring case and accents 

### Installation

Copy `_plugins/hash_table_sort_filters.rb` into the `_plugins` directory of Jekyll site working directory and restart the Jekyll server.

### Usage

Example 1: display a menu containing the blog's categories, sorted alphabetically. 
```
<ul>
  {% assign categories = site.categories | sort_by_keys %}
  {% for category in categories %}
    <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
  {% endfor %}
</ul>
```

Example 2: the same menu, sorted by the post count in the category. 
```
<ul>
  {% assign categories = site.categories | sort_by_content_size %}
  {% for category in categories %}
    <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
  {% endfor %}
</ul>
```